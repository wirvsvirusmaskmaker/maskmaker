<?php
/*
Plugin Name: MaskMaker Helped Me
Plugin URI: http://appmotivate.com
Description: A plugin to enable you to say that a request was fulfilled
Version: 1.0
Author: Peter Evans
Author URI: http://appmotivate.com
License: all rights reserved
*/

define('HELP_NEEDED_CATEGORY', 'bedarfsmeldung');
define('HELPED_CATEGORY', 'abgeschlossene-bedarfsmeldung');
define('HELPED_CATEGORY_ID', 40);

class UserHelpedMe {
    public $meta_name = 'maskmaker_helped_me_request';
    public $user_id;
    public $user_meta;
    public $request_id;
    function __construct($user_id, $request_id) {
        $this->user_id = $user_id;
        $this->request_id = $request_id;
    }
    public function userWasHelpedAlready() {
        _logIt('User Meta');
        _logIt($this->user_meta);
        $this->user_meta = get_user_meta($this->user_id, $this->meta_name);
        if (!is_array($this->user_meta))
            return false;
        for ($i = 0; $i < count($this->user_meta); $i++)
            if (intval($this->user_meta[$i]) == $this->request_id)
                return true;
        return false;
    }
    public function addRequest() {
        if ($this->userWasHelpedAlready()) {
            return false;
        }
        add_user_meta($this->user_id, $this->meta_name, $this->request_id);
        clean_user_cache($this->user_id);
        return true;
    }
}
/**
 * Include jQuery javascript
 * @credit http://css-tricks.com/snippets/wordpress/include-jquery-in-wordpress-theme/
 */
if (!is_admin())
    add_action("wp_enqueue_scripts", "maskmaker_helped_me_scripts_enqueue", 11);

function maskmaker_helped_me_scripts_enqueue() {
    maskmaker_basic_scripts_enqueue();
    $is_request = in_category(HELP_NEEDED_CATEGORY);
    if ($is_request) {
        maskmaker_rel_script('helped-me', __FILE__, array('jquery.cookie'), "1.0.3", 'wp_helped_me_vars', array(
            'currentUserId' => get_current_user_id(),
            'message' => __("Danke! Das macht uns bei MaskMaker und unserem Community sehr glücklich!"),
            'requestId' => get_the_ID(),
        ));
    }
}

function maskmaker_helped_me_button($request_id) {
    $nonce = wp_create_nonce('helped_me_nonce');
    $button = '
        <input id="helped-me-button" class="maskmaker-button submit" type="submit" data-requestid="'.$request_id.'" data-nonce="'.$nonce
            .'" data-message="'.__('Bedarf erfüllt').'" value="'.__('Bedarf erfüllt').'">'
        .'</input>';
    return $button;
}

add_filter('the_content', 'maskmaker_render_helped_me_button', 20);
function maskmaker_render_helped_me_button($content) {
    global $post;
    // $dbg = '<div class="debug"> home = '.get_option('home').'</div>'
    // . '<div class="debug"> prefix = '.maskmaker_get_path_prefix().'</div>'.'</br>';
    // $content = $dbg . $content;
    if (!is_single()) return $content;
    if (in_category(HELPED_CATEGORY)) {
        $helped_me = '
            <div id="helped-me-div" class="maskmaker-instruction">
                <span id="helped-me-done" class="helped-me-explain">Diese Bedarfsmeldung ist abgeschlossen.</span>
            </div><br/>';
        $content = $helped_me . $content;
        return $content;
    }
    if (!in_category(HELP_NEEDED_CATEGORY)) {
        return $content;
    }
    if (!is_user_logged_in()) return $content;
    $user_id = get_current_user_id();
    $author_id = $post->post_author;
    _logIt('maskmaker_render_helped_me_button user_id = '.$user_id.' author_id = '.$author_id);
    // Only author or admin can say that their need was fulfilled
    if ($user_id != $author_id) {
        if ( !current_user_can( 'delete_others_posts' ) && !current_user_can('moderate_comments'))
            return $content;
    }
    $request_id = get_the_ID();
    _logIt('maskmaker_render_helped_me_button request_id = '.$request_id);
    $user_meta = new UserHelpedMe($user_id, $request_id);
    if ($user_meta->userWasHelpedAlready()) {
        // FIXME if already fulfilled then it should be different category
        return $content;
    }
    $helped_me = '
        <div id="helped-me-div" class="maskmaker-instruction">
            <span id="helped-me-explain-1" class="helped-me-explain">Ist Ihr Bedarf schon erfüllt, so klicken Sie bitte hier</span>
            <span id="helped-me-button-span">'.maskmaker_helped_me_button(get_the_ID()).'</span><br/>
            <span id="helped-me-explain-2" class="helped-me-explain">(Nur der Ersteller dieses Beitrages sieht diesen Button)</span>
        </div><br/>';
    $content = $helped_me . $content;
    return $content;
}

function maskmaker_get_helped_me_count($request_id = null) {
    if ($request_id == null) {
        $request_id = get_the_ID();
    }
    //$helped_me = get_post_meta($request_id, 'maskmaker_helped_me_count', true);
    $helped_me = get_post_meta($request_id, 'maskmaker_helped_me_count', true);
    _logIt('maskmaker_get_helped_me_count for request_id '.$request_id.' count = '.$helped_me);
    if ($helped_me == '') {
        // No data
        return 0;
    }
    $helped_me = intval($helped_me);
    return $helped_me;
}
function maskmaker_render_helped_me_count() {
    $content = '';
    if (!is_single()) return $content;
    if (!in_category(HELP_NEEDED_CATEGORY)) return $content;
    $helped_me = maskmaker_get_helped_me_count();
    _logIt('maskmaker_render_helped_me_count count = '.$helped_me);
    if ($helped_me == 0) {
        return $content;
    }
    $content .= '<div id="maskmaker-helped-me" class="maskmaker maskmaker-happy">';
    $content .= '<h3>Mein Bedarf von '.$helped_me.' Masken wurde erfüllt! Vielen Dank!</h3>';
    $content .= '</div>';
    _logIt('maskmaker_render_helped_me_count returning: '.$content);
    return $content;
}
add_filter('the_content', 'maskmaker_render_helped_me', 20);
function maskmaker_render_helped_me($content) {
    if (!is_single()) return $content;
    if (!in_category(HELPED_CATEGORY)) return $content;
    $content = maskmaker_render_helped_me_count() . $content;
    return $content;
}

function maskmaker_increment_helped_me($current_user_id, $request_id) {
    $helped_me = maskmaker_get_helped_me_count($request_id);
    $helped_me++;
    _logIt('maskmaker_increment_helped_me count after increment = '.$helped_me);
    $user_meta = new UserHelpedMe($current_user_id, $request_id);
    if ($user_meta->userWasHelpedAlready()) {
        return;
    }
    update_post_meta($request_id, 'maskmaker_helped_me_count', $helped_me);
    clean_post_cache($request_id);
    $user_meta->addRequest();
}

add_action('wp_ajax_maskmaker_helped_me', 'maskmaker_handle_helped_me');
function maskmaker_handle_helped_me() {
    // Expect POST Parameters
    // current_user_id
    // request_id
    $nonce = isset($_POST['nonce']) ? $_POST['nonce'] : false;
    if (!maskmaker_good_nonce($nonce, 'helped_me_nonce')) {
        die("Naughty $nonce helped_me_nonce!");
    }
    _logIt('maskmaker_handle_helped_me processing ');
    _logIt($_POST);
    $current_user_id = intval($_POST['current_user_id']);
    $request_id = intval($_POST['request_id']);
    maskmaker_increment_helped_me($current_user_id, $request_id);
    wp_set_post_categories($request_id, [HELPED_CATEGORY_ID]);
    clean_post_cache($request_id);
    echo json_encode(array("status" => "ok"));
    die();
}
add_action('before_delete_post', 'maskmaker_remove_helped_me_count', 5);
function maskmaker_remove_helped_me_count() {
    $my_id = get_the_ID();
    _logIt("maskmaker_remove_helped_me_count $my_id");
    delete_post_meta($my_id, 'maskmaker_helped_me_count');
    return true;
}
/* stats */

add_shortcode('maskmaker_helped_me_stats', 'helped_me_stats');
function helped_me_stats() {
    global $wpdb;
    $ret = '';
    $results = $wpdb->get_results('SELECT * FROM v_maskmaker_helped_me_stats', ARRAY_A);
    if (empty($wpdb)) {
        $ret .= '<div>Bisher kein Bedarf erfüllt</div>';
        return $ret;
    }
    $ret .= '<div><table>';
    $len = count($results);
    for ($row = 0; $row < $len; $row++) {
        $columns = $results[$row];
        if ($row == 0) {
            $ret .= '<thead>';
            $ret .= helped_me_stats_header($columns);
            $ret .= '</thead>';
            $ret .= '<tbody>';
        }
        $ret .= '<tr>';
        foreach ($columns as $name => $value) {
            $ret .= '<td>';
            $ret .= $value;
            $ret .= '</td>';
        }
        $ret .= '</tr>';
    }
    $ret .= '</tbody>';
    $ret .= '</table></div>';
    return $ret;
}
function helped_me_stats_header($columns) {
    $ret = '<tr>';
    foreach ($columns as $name => $value) {
        $ret .= '<td>';
        $ret .= $name;
        $ret .= '</td>';
    }
    $ret .= '</tr>';
    return $ret;
}
