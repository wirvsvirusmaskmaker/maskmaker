/**
 * helped-me.js
 * Everything specfic to dealing with requests
 * (c) Peter Evans 2014
 */
jQuery(document).ready(function ($) {
    function maskmaker_helped_me_post(data, callback) {
        console.log('maskmaker_helped_me_post '+JSON.stringify(data));
        $.post(ajaxurl, data, function (response) {
            console.log('maskmaker_helped_me_post callback got response = '+response);
            if (callback) {
                callback(response);
            }
        });
    }
    if (!wp_helped_me_vars) {
        console.log('helped-me.js: no wp_helped_me_vars!');
        return;
    }
    console.log('helped-me.js wp_helped_me_vars = '+JSON.stringify(wp_helped_me_vars));
    $("#helped-me-button").one('click', function() {
        if (!wp_helped_me_vars) {
            console.log('helped-me: no wp_helped_me_vars!');
            return;
        }
        console.log("Caught click on #helped-me-button");
        console.log("Nonce = "+$(this).data('nonce'));
        alert(wp_helped_me_vars.message);
        var data = {
            action : 'maskmaker_helped_me',
            current_user_id : parseInt(wp_helped_me_vars.currentUserId, 10),
            request_id : parseInt(wp_helped_me_vars.requestId, 10),
            nonce : $(this).data('nonce')
        };
        maskmaker_helped_me_post(data, function (response) {
            console.log('maskmaker_helped_me_post callback response: '+response);
            location.reload(true);
        });
    });
});
