<?php
/*
Plugin Name: MaskMaker Redirect after Login
Plugin URI: http://maskmaker.de
Description: A plugin to enable users to be redirected to home page after login
Version: 1.0
Author: Peter Evans
Author URI: http://maskmaker.de
License: all rights reserved
*/

// add_filter( 'registration_redirect', 'maskmaker_redirect_home' );
// function maskmaker_redirect_home( $registration_redirect ) {
    // return home_url();
// }

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */
function maskmaker_login_redirect( $redirect_to, $request, $user ) {
    //is there a user to check?
    if ( isset( $user->roles ) && is_array( $user->roles ) ) {
        //check for admins
        if ( in_array( 'administrator', $user->roles ) ) {
            // redirect them to the default place
            return $redirect_to;
        } else {
            return home_url();
        }
    } else {
        return $redirect_to;
    }
}
 
add_filter( 'login_redirect', 'maskmaker_login_redirect', 10, 3 );

