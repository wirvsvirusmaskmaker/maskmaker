<?php
/*
Plugin Name: MaskMaker Utility Library
Plugin URI: http://appmotivate.com
Description: A plugin containing functions needed by other MaskMaker plugins and tempplates
Version: 1.0
Author: Peter Evans
Author URI: http://appmotivate.com
License: all rights reserved
*/

/*
 * @credit http://wp.smashingmagazine.com/2011/09/30/how-to-create-a-wordpress-plugin/
 */

//ini_set('error_log');

class Maskmaker_counter {
	public $count = 0;
	public $pos = 0;
	function Counter($max) {
		$this->count = $max;
	}
	function incr() {
		$this->pos++;
	}
	function reset() {
		$this->pos = 0;
		return $this;
	}
	function last() {
		return $this->pos == $this->count;
	}
}

if (!function_exists('_logIt')) {
    function _logIt( $message ) {
		if (WP_DEBUG) {
		    if (is_array($message) || is_object($message)) {
				error_log(print_r($message, true));
		    } else {
				error_log($message);
		    }
		}
    }
}

$maskmaker_site_url = get_site_url();

if (!function_exists('maskmaker_get_site_url')){
	function maskmaker_get_site_url() {
		global $maskmaker_site_url;
		return $maskmaker_site_url;
	}
}

/**
 * @credit http://www.ghosthorses.co.uk/production-diary/adding-cropping-support-for-wordpress-medium-thumbs/
 */
if(!get_option("medium_crop"))
	add_option("medium_crop", "1");
else
	update_option("medium_crop", "1");

function maskmaker_good_nonce($nonce, $nonce_id) {
	_logIt('maskmaker_good_nonce');
	if (!$nonce) {
		_logIt("maskmaker_good_nonce $nonce_id: no nonce!");
		return false;
	}
	if (!wp_verify_nonce($nonce, $nonce_id)) {
		_logIt("maskmaker_good_nonce: invalid nonce $nonce for $nonce_id!");
		return false;
	}
	return true;
}

/**
 * @credit http://www.tcbarrett.com/2013/05/wordpress-how-to-get-the-slug-of-your-post-or-page/#.UtY7YXnfYqc
 */
function maskmaker_get_the_slug($id=null) {
	if (empty($id)) {
		global $post;
		if (empty($post))
		return ''; // No global $post var available.
		$id = $post->ID;
	}
	$slug = basename(get_permalink($id));
	return $slug;
}

function maskmaker_get_path_prefix() {
	$home = get_option('home');
	$prefix = preg_replace('/^https?:\/\/[^\/]+\/?/', '', $home);
	if ($prefix != '')
		$prefix = '/'.$prefix;
	return $prefix;
}

/**
 * get just the path part of the permalink (without the domain and protocol part)
 *
 * @credit http://wordpress.org/support/topic/permalinks-without-domain-part (Aleksandersen)
 */
function maskmaker_permalink_path($id) {
    return substr(get_permalink($id), strlen(get_option('home')));
}
function maskmaker_abs_dir($file = '') {
	if ($file == '')
		$file = __FILE__;
	$this_dir_name = basename(dirname($file));
    return substr(plugin_dir_url(plugin_dir_path($file)), strlen(get_option('home'))).$this_dir_name;
}
function maskmaker_get_url() {
    $id = get_the_ID();
    $permalink = maskmaker_permalink_path($id);
    return $permalink;
}

/**
 * Encapsulate script deregistration, registration, enueueing and localizing in one place
 */
function maskmaker_rel_script($handle, $src_or_plugin_file=null, $deps=false, $version=null, $bridgeName="wp_vars", $bridgeVars=array()) {
    wp_deregister_script($handle);
	if ($src_or_plugin_file === null) {
		$src_or_plugin_file = __FILE__;
	}
	if (strpos($src_or_plugin_file,'http') === 0) {
		// we have a URL
		$src = $src_or_plugin_file;
	} else {
		// we have a filename whose directory is to be found
		// E.g. /var/www/html/wp-content/plugins/my_plugin/my_plugin.php
		// If handle is "myjsfile"
		// The generated filename will be /wp-content/plugins/my_plugin/js/myjsfile.js
	    $js = maskmaker_abs_dir($src_or_plugin_file) . "/js";
		$src = "$js/$handle.js";
	}
	wp_register_script($handle, $src, $deps, $version);
	wp_enqueue_script($handle);
	if (!empty($bridgeVars)) {
		wp_localize_script($handle, $bridgeName, $bridgeVars);
	}
}

/**
 * Include jQuery javascript
 * @credit http://css-tricks.com/snippets/wordpress/include-jquery-in-wordpress-theme/
 */
if (!is_admin())
     add_action("wp_enqueue_scripts", "maskmaker_basic_scripts_enqueue", 11);

$maskmaker_basic_scripts_enqueued = false;
function maskmaker_basic_scripts_enqueue() {
	global $maskmaker_basic_scripts_enqueued;
	if ($maskmaker_basic_scripts_enqueued) {
		return;
	}
	$jqueryUrl = "http".($_SERVER['SERVER_PORT'] == 443 ? "s" : "")."://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js";
	//maskmaker_rel_script('jquery', $jqueryUrl, false, null);
	//maskmaker_rel_script('jquery.cookie', __FILE__, array('jquery'), "1.0.0");
	maskmaker_rel_script('jquery.cookie', __FILE__, false, "1.0.0");
	//maskmaker_rel_script('common', __FILE__, array('jquery'), "1.0.0");
	maskmaker_rel_script('common', __FILE__, false, "1.0.0");
	$maskmaker_basic_scripts_enqueued = true;
}
function maskmaker_get_post_authorname($post_id) {
	if (!$author_id = get_post_field('post_author', $post_id))
		return false;
	if (!$author = get_userdata($author_id))
		return false;
	return $author->user_login;
}

/**
 * Check for special single post category template
 * Caveat: if the post belongs to more than one category
 * for which custom templates are defined, then the first one
 * found will be used. Which one is undefined.
 *
 * @credit https://halgatewood.com/wordpress-custom-single-templates-by-category/
 */
//add_filter('single_template', 'check_for_category_single_template');
function check_for_category_single_template($t) {
    $templatePath = plugin_dir_path(__FILE__);
    foreach ((array) get_the_category() as $cat) {
		$fileName = $templatePath . "single-category-{$cat->slug}.php";
		if (file_exists($fileName))
		    return $fileName;
		if ($cat->parent) {
		    $cat = get_the_category_by_ID($cat->parent);
			$fileName = $templatePath . "single-category-{$cat->slug}.php";
		    if (file_exists($fileName))
		        return $fileName;
		}
    }
    return $t;
}
add_filter('the_content', 'maskmaker_excerpts_only_in_non_single');
function maskmaker_excerpts_only_in_non_single($content = '') {
	if (is_single()) return $content;
	if (is_admin()) return $content;
	if (!is_home()) return $content;
	global $post;
	//_logIt('excerpt = '.$post->post_excerpt);
	if (!empty($post->post_excerpt))
		$content = $post->post_excerpt;
	if (strlen($content) < 400)
		$out = $content;
	else
		$out = mb_substr(strip_tags($content), 0, 400) . ' [...]';
	//$out = strip_tags($out);
	return $out;
}
/**
 * Creates a list of position classes to make it easy easy to add striping effects
 * or styling or positional changes on n-th elements in a list
 * It shoud be called iteratively when rendering a list of posts e.g.
 * When count == 1, class pos-first is added
 * When count == entry_count, class pos-last is added
 * The fourth entry in a list of recent posts would generate the following classes:
 * boxed-posts recent-posts pos-2-of-2 pos-last-of-2 pos-1-of-3 pos-4-of-4 pos-last-of-4 pos-4-of-5 pos-4-of-6 pos-4-of-7 pos-4-of-8
 * @param $count - position of element in list
 * @param $entry_count - total number of elements in list
 * @param $prefix - something meaningful, e.g."recent", "featured", "important"
 * @param $type - element type, e.g. "post", "author"
 */
function maskmaker_prepare_pretty_classes($count, $entry_count, $prefix='a', $type='posts') {
	$classes = "feature columns boxed-post {$prefix}-$type";
	if ($count == 1) {
		$classes .= " pos-first";
	}
	for ($i = 2; $i <= 8; $i++) {
		$pos = $count % $i;
		if ($pos == 0) $pos = $i;
		$classes .= ' ' . 'pos-' . $pos . '-of-' . $i;
		if ($i == $pos) {
			$classes .= ' ' . 'pos-last-of-' . $i;
		}
	}
	if ($count == $entry_count) {
		$classes .= ' ' . "pos-last";
	}
	return $classes;
}

function maskmaker_pretty_posts($query_args, $control_args) {
	global $post;
	global $maskmaker_pretty_counter;
	$maskmaker_process_excerpts = true;
	$out = '';
	$posts = new WP_Query($query_args);
	//$out = 'Number of posts = '.$posts->found_posts;
	$css_classes = isset($control_args['wrapper_classes']) ? 'maskmaker' : $control_args['wrapper_classes'];
	if (!$posts->have_posts()) {
		$out = '<div class="maskmaker-sad">There are currently no posts of this kind.</div>';
	} else {
		$count = 0;
		while ($posts->have_posts()) {
			$count++;
			$posts->the_post();
			$classes = maskmaker_prepare_pretty_classes($count, $posts->found_posts, $control_args['prefix'], $control_args['type']);
			$out .= maskmaker_do_pretty_post_div($post, $classes, $count, $control_args['prefix']);
		}
	}
	return $out;
}
/**
 * maskmaker_get_first_image
 * return the first attached image, if any
 * If none is found, it parses the DOM to find the first used image, if any, and returns that
 * Otherwise returns false
 * NB Images are attached once to one post only.  If reused in another post they will not be returned by get_children()
 * Thanks due to http://stackoverflow.com/users/1347308/maiorano84,
 * see http://stackoverflow.com/questions/17970304/get-images-used-in-but-not-attached-to-page
 */
function maskmaker_get_first_image($the_id) {
    $attached_image = get_children( "post_parent={$the_id}&post_type=attachment&post_mime_type=image&numberposts=1" );
    _logIt("maskmaker_get_first_image $the_id");
    if ($attached_image) {
	foreach ($attached_image as $attachment_id => $attachment) {
	    $url = wp_get_attachment_url($attachment_id);
	    _logIt("maskmaker_get_first_image attached image found $url");
	    return $url;
	}
    }
    // no attached image, so find used image
    $dom = new DOMDocument();
    $content = get_post_field('post_content', $the_id);
    _logIt('maskmaker_get_first_image content = |'.$content.'|');
    $dom->loadHTML($content);
    $images = $dom->getElementsByTagName('img');
    if (!$images) {
        return false;
    }
    foreach($images as $img){
	$src = $img->getAttribute('src');
	_logIt('maskmaker_get_first_image src = '.$src);
	// return first image found
	return $src;
    }
    return false;
}

function maskmaker_featured_or_first_image() {
    if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
	    the_post_thumbnail('medium');
    } else {
		$src = maskmaker_get_first_image(get_the_ID());
		if ($src) {
			$first_image = '<img src="'.$src.'" />';
			echo $first_image;
		}
    }
}

function maskmaker_do_pretty_post_div($the_post, $classes, $count, $prefix) {
	$output = '';
	_logIt("maskmaker_do_pretty_post_div");
	$post_title = get_the_title($the_post->ID);
	_logIt("maskmaker_do_pretty_post_div $post_title");
	if (strlen($post_title) > 52) $post_title = substr($post_title, 0, 46).' [...]';
	$author = maskmaker_get_post_authorname($the_post->ID);
	$author_url = maskmaker_get_path_prefix().'/members/'.$author;
	$classes .= ' four';
	$pic = '';
	if (has_post_thumbnail($the_post->ID)) {
		$thumb = get_the_post_thumbnail($the_post->ID, 'medium');
		_logIt('thumb = '.$thumb);
		$pic = '<div class="featured-image">'.$thumb.'</div>';
		$classes .= ' has-thumbnail';
	} else {
	    $src = maskmaker_get_first_image($the_post->ID);
	    if ($src) {
		$first_image = '<img src="'.$src.'" />';
		$pic = '<div class="featured-image">'.$first_image.'</div>';
		$classes .= ' has-thumbnail';
	    } else {
		$pic = '<div class="featured-image">'
			 . '<img src="'.get_stylesheet_directory_uri().'/images/maskmaker-slant.png" /></div>';
		$classes .= ' has-no-thumbnail';
	    }
	}
	$text = '<div id="'.$prefix.'-post-'.$count.'" class="'.$classes.'">'
		. $pic
		. '<a href="' . get_permalink($the_post->ID) . '" title="'. esc_attr($post_title) . '" >'
		.   '<h4 class="maskmaker-title">' . $post_title . '</h4></a>'
		. '<div class="maskmaker-title-link" >'
		. '<p>'
		. ' by '
		. '<a href="'.$author_url.'"><span class="username">'.$author.'</span>' . '</a>'
		. '</p>'
		. '</div>'
		. '</div>';
	$output .= $text;
	return $output;
}
function maskmaker_handle_username_error($s, $msg, $messages = []) {
	array_push($messages, $msg);
	return $messages;
}
function maskmaker_validate_username($username, $messages) {
	if (strlen($username) > 30) {
		// too long
		return maskmaker_handle_username_error('user_name', __( 'Der Benutzername darf nicht länger als 30 Zeichen sein'), $messages);
	}
	if(strlen($username) < 4) {
		// too short
		return maskmaker_handle_username_error('user_name', __( 'Der Benutzername muss mindestens 4 Zeichen enthalten'), $messages);
	}
	if(!preg_match('/^[\w-]+$/', $username)) {
		// there are non-alphanumerics
		return maskmaker_handle_username_error('user_name', __( 'Der Benutzername darf keine Leerzeichen bzw. Spezialzeichen enthalten'), $messages);
	}
	$bad_users = "about access account accounts add address adm admin administrator administration adult advertising affiliate affiliates ajax analytics android anon anonymous api app apps archive atom auth authentication avatar backup banner banners bin billing blog blogs board bot bots business chat cache cadastro calendar campaign careers cgi client cliente code comercial compare config connect contact contest create code compras css dashboard data db design delete demo design designer dev devel dir directory doc docs domain download downloads edit editor email ecommerce forum forums faq favorite feed feedback flog follow file files free ftp gadget gadgets games guest group groups help home homepage host hosting hostname html http httpd https hpg info information image img images imap index invite intranet indice ipad iphone irc java javascript job jobs js knowledgebase log login logs logout list lists mail mail1 mail2 mail3 mail4 mail5 mailer mailing main mx manager marketing master me media message microblog microblogs mine mp3 msg msn mysql messenger mob mobile movie movies music musicas my name named net network new news newsletter nick nickname notes noticias ns ns1 ns2 ns3 ns4 old online operator order orders page pager pages panel password perl pic pics photo photos photoalbum php plugin plugins pop pop3 post postmaster postfix posts profile project projects promo pub public python random register registration root ruby rss sale sales sample samples script scripts select secure send service shop sql signup signin search security settings setting setup site sites sitemap smtp soporte ssh stage staging start subscribe subdomain suporte support stat static stats status store stores system tablet tablets tech telnet test test1 test2 test3 teste tests theme themes tmp todo task tasks tools tv talk update upload url user username usuario usage vendas video videos visitor win ww www www1 www2 www3 www4 www5 www6 www7 wwww wws wwws web webmail website websites webmaster workshop xxx xpg you yourname yourusername yoursite yourdomain";
	$bad_users_list = explode(' ', $bad_users);
	if (in_array($username, $bad_users_list)) {
		return maskmaker_handle_username_error('user_name', __( 'Schlechter Benutzername'), $messages);
	}
	return $messages;
}
//add_filter('pre_user_login', 'maskmaker_validate_username');
add_filter('user_registration_response_array', 'maskmaker_validate_ur_formdata', 10, 3);
//add_filter('user_registration_before_register_user_filter', 'maskmaker_validate_username', 10, 3);
function maskmaker_validate_ur_formdata($messages, $form_data = [], $form_id = -1) {
	error_log('maskmaker_validate_username: form_data: '.print_r($form_data, TRUE));
	error_log('maskmaker_validate_username: '.json_encode([$messages, $form_data, $form_id], JSON_PRETTY_PRINT));
	for ($i = 0; $i < count($form_data); $i++) {
		$element = $form_data[$i];
		if ($element->field_name === 'user_login') {
			$username = $element->value;
			$messages = maskmaker_validate_username($username, $messages);
			return $messages;
		}
	}
	return $messages;
}
add_filter( 'wp_title', 'maskmaker_wp_title' );
function maskmaker_wp_title( $title ) {
	$out = get_bloginfo( 'name' );
	if (!empty($title)) {
		$out .= ' | '.$title;
  	}
	return $out;
}

add_action('wp_head', 'maskmaker_ajaxurl');

function maskmaker_ajaxurl() {
   echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}

add_filter('comment_notification_text', 'maskmaker_clean_personal_info');

function maskmaker_clean_personal_info($notify_message) {
	$notify_message_a = explode("\n",$notify_message);
	$out = [];
	foreach ($notify_message_a as $k => $line) {
		$header = trim(substr($line,0,strpos($line,':')));
		switch ($header) {
			case 'E-mail':
			case 'E-Mail':
				//unset($notify_message_a[$k]);
				break;
			case 'URL' :
			case 'Whois':
				// unset($notify_message_a[$k]);
				break;
			case 'Author' :
			case 'Autor' :
				$pat = '([^(]+)\(.*$';
				//$notify_message_a[$k] = trim(preg_replace('|'.$pat.'|','$1',$line));
				$out[] = trim(preg_replace('|'.$pat.'|','$1',$line));
				break;
			default:
				$out[] = $line;
		}
	}
	$notify_message = implode("\n", $out);
	return $notify_message;
}
