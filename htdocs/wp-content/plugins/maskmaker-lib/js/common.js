/**
 * @author Peter Evans
 */

function maskmaker_post(data, callback) {
	console.log('maskmaker_post '+JSON.stringify(data));
	jQuery.post(ajaxurl, data, function (response) {
		console.log('maskmaker_post callback got response = '+response);
		if (callback) {
			callback(response);
		}
	});
}
jQuery(document).ready(function ($) {
	// get rid of class and id on buddypress' default wrapping divs
	// as we don't want them styled
	$("#contextual-help-link").click(function () {
        $("#contextual-help-wrap").css("cssText", "display: block !important;");
    });
    $("#show-settings-link").click(function () {
		$("#screen-options-wrap").removeAttr("class");
        $("#screen-options-wrap").css("cssText", "display: block !important;");
	});
	setTimeout(function() {
		$("#contextual-help-link").click(function () {
			$("#contextual-help-wrap").removeClass('hidden');
			$("#contextual-help-wrap").css("cssText", "display: block !important;");
		});
		$("#show-settings-link").click(function () {
			$("#screen-options-wrap").removeClass('hidden');
			$("#screen-options-wrap").css("cssText", "display: block !important;");
		});	
	}, 1000);
	$(".padder").removeAttr("class");
	$("#content").removeAttr("id");
	$("#searchform #s").attr('placeholder', 'Search Site ...');
});
